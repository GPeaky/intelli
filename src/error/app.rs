use super::{user::UserError, ChampionshipError, CommonError, SocketError, TokenError};
use crate::response::AppErrorResponse;
use axum::http::StatusCode;
use axum::response::{IntoResponse, Response};
use thiserror::Error;
use tracing::info;

pub type AppResult<T> = Result<T, AppError>;

#[derive(Debug, Error)]
pub enum AppError {
    #[error(transparent)]
    User(#[from] UserError),
    #[error(transparent)]
    Championship(#[from] ChampionshipError),
    #[error(transparent)]
    Token(#[from] TokenError),
    #[error(transparent)]
    Common(#[from] CommonError),
    #[error(transparent)]
    Socket(#[from] SocketError),
    #[error(transparent)]
    Database(#[from] sqlx::Error),
}

impl IntoResponse for AppError {
    fn into_response(self) -> Response {
        match self {
            AppError::User(e) => e.into_response(),
            AppError::Championship(e) => e.into_response(),
            AppError::Token(e) => e.into_response(),
            AppError::Common(e) => e.into_response(),
            AppError::Socket(e) => e.into_response(),
            AppError::Database(e) => {
                info!("{e}");

                AppErrorResponse::send(
                    StatusCode::INTERNAL_SERVER_ERROR,
                    Some("Database Error".to_owned()),
                )
            }
        }
    }
}
