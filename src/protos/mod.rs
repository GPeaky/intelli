pub(crate) mod car_motion_data;
pub(crate) mod event_data;
pub(crate) mod final_classification;
pub(crate) mod participants;
pub(crate) mod session_data;
pub(crate) mod session_history;

include!(concat!(env!("OUT_DIR"), "/protos.packet_header.rs"));
